const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta charset="utf-8" />
		<meta
			name="viewport"
			content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
		/>
		<link
			rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
			integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
			crossorigin="anonymous"
		/>
		<title>ESP Input Form</title>
	</head>
	<body
		onload="bsCustomFileInput.init()"
		style="background: #0d0e0e; color: #dddad6;"
	>
		<div class="container">
			<h1 class="display-4">Load a program into CBT.</h1>
			<div class="row rows-col-1">
				<form class="col-md" method="POST" action="/upload_hex">
					<div class="row row-cols-1">
						<h3 class="col">In plain text hex:</h3>
						<div class="col">
							<textarea
								style="
									height: 150px;
									width: 355px;
									background: #131516;
									color: #dddad6;
									border-color: #3c4144;
									border-radius: 5px;
								"
								placeholder="eg. de ad be ef"
								name="hex"
							></textarea>
						</div>
					</div>
					<button
						type="submit"
						style="margin-top: 15px;"
						class="btn btn-primary col-md-2"
					>
						Upload
					</button>
				</form>
				<form
					class="col-md"
					method="POST"
					action="/upload_bin"
					enctype="multipart/form-data"
				>
					<div class="row row-cols-1">
						<h3 class="col">or with a <code>.bin</code> file:</h3>
						<div class="col custom-file">
							<input
								type="file"
								accept=".bin"
								name="bin"
								class="custom-file-input"
								id="inputGroup"
							/>
							<label
								class="custom-file-label col-md-6"
								for="inputGroup"
								style="
									background: #181a1b;
									color: #b5afa6;
									border-color: #3c4144;
									margin-left: 15px;
								"
								>Choose file
							</label>
						</div>
					</div>

					<button
						type="submit"
						style="margin-top: 15px;"
						class="btn btn-primary col-md-2"
					>
						Upload
					</button>
				</form>
			</div>
		</div>
		<style>
			.custom-file-label::after {
				background: #232627;
				color: #b5aea4;
			}
			h1 {
				margin: 20px 20px 20px 0;
			}
		</style>
		<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
	</body>
</html>

)rawliteral";