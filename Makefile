all:
	platformio -f -c nvim run

upload:
	sed -i "s/ssid = \"\"/ssid = \"$$SSID\"/g" src/esp32-as-rom.cpp
	sed -i "s/pass = \"\"/pass = \"$$WIFI_PASS\"/g" src/esp32-as-rom.cpp
	platformio -f -c nvim run --target upload
	sed -i "s/ssid = \".*\"/ssid = \"\"/g" src/esp32-as-rom.cpp
	sed -i "s/pass = \".*\"/pass = \"\"/g" src/esp32-as-rom.cpp

clean:
	platformio -f -c nvim run --target clean

program:
	platformio -f -c nvim run --target program

uploadfs:
	platformio -f -c nvim run --target uploadfs

update:
	platformio -f -c nvim update
