# ESP32 as rom
Emulate EEPROM with ESP32. It saves the given program in onboard EEPROM.

## PIO
This project uses Platform IO which means it's neccessary to have `platformio` installed.

## Usage
1. Set `SSID` and `WIFI_PASS` enviromental variables to their respective values.
2. Run `make upload` to compile and upload.

I used the default partitioning scheme which seems to allow for about 4096B (above that it wasn't reliable),
it is possible to have a higher value however it might be neccessary then to increase the nvs partition.