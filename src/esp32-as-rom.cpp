#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <EEPROM.h>
#include <ESPAsyncWebServer.h>

#include "index_html.h"

#define ADDRESS_WIDTH 10
#define ROM_ENABLE 13
#define CPU_RST 12

// needs to be smaller than 0x5000 with default partitioning
// for now this works and seems to be enough
#define EEPROM_SIZE 4096
// MSB has index 0
const unsigned int ADDRESS_INPUTS[ADDRESS_WIDTH] = {34,35,15,2,4,16,17,5,18,19};
const unsigned int DATA_OUTPUTS[8] = {14,26,25,33,32,21,22,23};

// to set ssid and wifi password you should use environmental variables
// by default they are SSID and WIFI_PASS respectively
// you can change them in Makefile
const char *ssid = "";
const char *pass = "";

AsyncWebServer server(80);

void setData(unsigned int address){
	// assert 1 or pull down
	for(unsigned int i=0; i<8; i++){
		if (EEPROM.read(address)&(1<<i)){
			pinMode(DATA_OUTPUTS[7-i], OUTPUT);
			digitalWrite(DATA_OUTPUTS[7 - i],1);
		}
		else{
			pinMode(DATA_OUTPUTS[7-i], INPUT_PULLDOWN);
			digitalWrite(DATA_OUTPUTS[7 - i],0);
		}
	}
}

void handleUpload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final){
	for (size_t i = 0; i < len; i++){
		EEPROM.write(i, data[i]);
	}
	if (final){
		if (index + len < EEPROM_SIZE) {
			if (EEPROM.commit())
				request->send(200, "text/html","Successfully uploaded " + String(filename.c_str()));
			else 
				request->send(200, "text/html", "Failed to upload!");
		}
		else
			request->send(200, "text/html","File exceeds maximum size of " + String(EEPROM_SIZE) + " bytes.");
	}
}

unsigned int currentAddress = 0;

void setup(){
    Serial.begin(115200);

    // address
	for (uint i = 0; i < ADDRESS_WIDTH; i++){
		pinMode(ADDRESS_INPUTS[i], INPUT_PULLDOWN);
	}
    // data
	for (uint i = 0; i < 8; i++){
		pinMode(DATA_OUTPUTS[i], INPUT_PULLDOWN);
	}
	pinMode(ROM_ENABLE, INPUT_PULLUP);

	EEPROM.begin(EEPROM_SIZE);

	// wifi
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, pass);

	// Wait for connection
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.print("Connected to ");
	Serial.println(ssid);
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());

	// server
	server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
		request->send_P(200, "text/html", index_html);
	});

	server.on("/currentAddress", HTTP_GET, [](AsyncWebServerRequest* request){
		Serial.printf("Current address: %d\r\n", currentAddress);
		request->send(200, "text/html", "Current address: " + String(currentAddress));
	});

	server.on("/rst", HTTP_GET, [](AsyncWebServerRequest* request){
		pinMode(CPU_RST, OUTPUT);
		digitalWrite(CPU_RST, 0);
		pinMode(CPU_RST, INPUT_PULLUP);
		delayMicroseconds(1);
		Serial.printf("Reset whole cpu.\n");
		request->send(200, "text/html", "Reset whole cpu.");
	});

	server.on("/eepromDATA", HTTP_GET, [](AsyncWebServerRequest* request){
		for (int i = 0; i < EEPROM_SIZE; i++){
			Serial.printf("%02X ", byte(EEPROM.read(i)));

			if ((i+1)%16==0)
				Serial.println("");
		}
		Serial.println("");
		request->send(200, "text/html", "Check serial.");
	});

	server.on("/upload_hex", HTTP_POST, [](AsyncWebServerRequest* request){
		if (request->hasParam("hex", true)){
			String got = request->getParam("hex")->value();

			unsigned int addr = 0;
			String data = "";
			for (unsigned int i = 0; i <= got.length(); i++){
				if (got[i] == 32 || got[i] == 0 || got[i] == 10 || got[i] == 13){
					uint8_t t = strtoul(data.c_str(), NULL, 16);

					if (t)
						EEPROM.write(addr, t);
					
					else if(data == "00")
						EEPROM.write(addr, 0);
					
					else
						Serial.println("Couldn't interpret " + data + " as a hex number.");

					addr++;
					data = "";
				}
				else
					data += got[i];
				
			}
			if(EEPROM.commit()){
				setData(currentAddress);
				Serial.println("Succesfully saved data to EEPROM.");
				request->send(200, "text/html", "Succesfully saved data to EEPROM.");
			}else{
				Serial.println("Couldn't save data to EEPROM!");
				request->send(200, "text/html", "Couldn't save data to EEPROM!");
			}
		} else{
			Serial.println("No data has been provided.");
			request->send(200, "text/html", "No data has been provided.");
		}
	});

	server.on("/upload_bin", HTTP_POST, [](AsyncWebServerRequest* request){}, handleUpload);

	server.onNotFound([](AsyncWebServerRequest* request){
		request->send(404, "text/plain", "Not found");
	});

	server.begin();
	Serial.println("HTTP server started");
	setData(0);
}

unsigned int address;

void loop(){
	address = 0;
	for (uint8_t i = 0; i<ADDRESS_WIDTH; i++){
		address = address << 1;
		address += digitalRead(ADDRESS_INPUTS[i]);
	}
	if (digitalRead(ROM_ENABLE) != 0){
		for(unsigned int i=0; i<8; i++){
			pinMode(DATA_OUTPUTS[7-i], INPUT_PULLDOWN);
			digitalWrite(DATA_OUTPUTS[7-i],0);
		}
	} else if (address != currentAddress) {
	  currentAddress = address;
	  Serial.printf("Address changed to %d\r\n", currentAddress);
	  setData(address);
	}
	delayMicroseconds(1);
}
